
<?php include('logo.html'); ?>
<div class="bureau">

<?php
   $cat_args=array(
     'orderby' => 'date',
     'order' => 'ASC'
  );

   $numbForm = 1;
   $categories=get_categories($cat_args);
   foreach($categories as $cat){
      $catName = $cat->name;
      $params = array( 
	 'where'   => 'category.name LIKE "'.$catName.'"', 
      );
      $pods = pods('page_projet', $params);
      ?>
	 <div class="dossier" id="<?php echo $catName; ?>" data-numb="<?php bloginfo('template_url'); ?>/fond_form/forme<?php echo $numbForm; ?>_blanc.png" >
   <?php
	 $numbForm++;
	 $i = 0;
	 while ($pods->fetch()) {
	    $marginY = -11;
	    if($i < 6)
	    {
	       $marginX = $i * 15;
	    }else
	    {
	       $marginX = ((6 + 4 ) * 10) - ($i *11);
	    }
	     $titre = $pods->get_field('title');
	     $slug = $pods->get_field('slug');
	     $id = $pods->get_field('id');
	     $cate = $pods->field('category.id');
	     $color = $pods->get_field('vignette_couleur');
	     if(empty($color)){
		$color = 'white';
	     }
	 ?>
	    <a class="fiche title" id="fiche_<?php echo $slug; ?>" style="<?php echo 'margin-top:'.$marginY.'px; margin-left:'.$marginX.'px; background:'.$color.''; ?>" data-article="<?php bloginfo('url'); ?>/index.php/<?php echo $slug; ?>" data-category="<?php echo $catName; ?>" data-date="<?php echo $date_even; ?>" >
	    <div class="rectangle"><?php echo $titre; ?></div>
	    </a><br><br>
   <?php

	     $i++;
	 }

	 $marginX = $margin + 80;
	 // $marginY = $margin +100;
   ?>
      
   </div>
   <?php
   }
?>

</div>

<div class="menu">
   <?php include('menu.php'); ?>
</div>


