<!DOCTYPE html>
<html>
  <head <?php language_attributes(); ?>>
    <meta charset="<?php bloginfo('charset'); ?>">
    <title><?php bloginfo('name'); ?></title>
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css">
    <link rel='stylesheet' id='metaslider-flex-slider-css'  href='<?php bloginfo('url'); ?>/wp-content/plugins/ml-slider/assets/sliders/flexslider/flexslider.css?ver=3.3.6' type='text/css' media='all' property='stylesheet' />

    <script type="text/javascript" src="http://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.scrollTo.js"></script>
    <?php wp_head(); ?>
  </head>
<body>
<header>
   <?php
      include('logo.html');

      $params = array(
	 'orderby' => 'date DESC',
	 'limit' => -1 // Returns all
       );

      $menus = new pod('menu', $params);

      while ($menus->fetch()) {
	 $menu_name = $menus->get_field('title');
	 $menu_slug = $menus->get_field('slug');
	 if($menu_slug == 'a-propos'){
   ?>
      <span class="title" id="menu_<?php echo $menu_slug; ?>"  data-article="<?php bloginfo('url'); ?>/index.php/menu/<?php echo $menu_slug; ?>" ><?php echo $menu_name; ?></span>
   <?php
	 }
      }


      ?>
</header>
