<?php 
   $params = array(
      'orderby' => 'date DESC',
      'limit' => -1 // Returns all
    );

   $menus = new pod('menu', $params);

   while ($menus->fetch()) {
      $menu_name = $menus->get_field('title');
      $menu_slug = $menus->get_field('slug');
      if($menu_slug != 'a-propos'){
?>
   <span class="title" id="menu_<?php echo $menu_slug; ?>"  data-article="<?php bloginfo('url'); ?>/index.php/menu/<?php echo $menu_slug; ?>" ><?php echo $menu_name; ?></span>
<?php
      }
   }
?>
