 <div id="single-post post-<?php the_ID(); ?>">
   <?php
      $slug = pods_v( 'last', 'url'  );
      $projet_pod = new Pod('page_projet', $slug);
      $menu_pod = new Pod('menu', $slug);
      
      $titre = $projet_pod->get_field('title');
      $presentation = $projet_pod->get_field('content');
      $lieu = $projet_pod->get_field('lieu_evenement');
      $date = $projet_pod->get_field('date_evenement');
      $date = $date .' → ';
      $typo_color = $projet_pod->get_field('typo_couleur');
      $fond_color = $projet_pod->get_field('fond_couleur');



      $menu_titre= $menu_pod->get_field('title');
      $menu_presentation = $menu_pod->get_field('content');
      
      if(empty($fond_color)){
	 $fond_color = 'white';
      }
      if(empty($typo_color)){
	 $typo_color = 'black';
      }
      if($titre){
	 echo '<div class="cont_projet" style="color:' . $typo_color .'; background:' . $fond_color .  '" >';
	 echo '<div class="cont_head" ><div class="titre" id="titre">'.$titre.'</div>';
	 echo '<div class="date_lieu" >'.$date. $lieu . '</div></div>';
	 echo '<div class="presentation">'.$presentation.'</div>';
	 echo '</div>';
      }
      if($menu_titre){
	 echo '<div class="cont_menu">';
	    echo '<h1 class="titre" id="titre">'.$menu_titre.'</h1>';
	    echo '<div class="presentation">'.$menu_presentation.'</div>';
	 echo '</div>';
      }
   ?>
</div>

