<?php wp_footer(); ?>


<script>
$(document).ready(function(){
   $('.prevFiche').hide();
   $('#Non').remove();

   function resizePage(valResize){
      var valCont = 100 - valResize;
      $('.bureau').animate({width: valResize+'%'}, 'fast');
      $('.menu').animate({width: valResize+'%'}, 'fast');
      $('.logo').animate({width: valResize+'%'}, 'fast');
      $('header').animate({width: valResize+'%'}, 'fast');
      $('.container').animate({width: valCont+'%'}, 'fast');
   }

   function scrollBureau(slugName){
      var heightWind = $(window).height() - 50;
      var dossierValY = $('#fiche_'+slugName).parent().offset().top + heightWind - $('#fiche_'+slugName).parent().height();
      $('.bureau').scrollTo( dossierValY+'px', 400);
   }

   function loadLocation(){
      var startPost = window.location.hash;
      startPost = startPost.substring(1);
      var locationUrl = window.location.origin + window.location.pathname + 'index.php/' + startPost;

      if(startPost){
	$("#post").html('loading');
	$("#post").load(locationUrl);

	if(startPost == 'a-propos'){
	   resizePage(50);
	}else{
	   resizePage(25);
	   scrollBureau(startPost);
	}

      }else{
	 var locationUrl = window.location.origin + window.location.pathname + 'index.php/menu/a-propos/';
	 $("#post").html('loading');
	 $("#post").load(locationUrl);
	 resizePage(50);
      }
   }

   function loadPage(){

     $.ajaxSetup({cache:false});

     $(".title").click(function(){
	var scroSave = $('.bureau').scrollTop();
	// Add Single page to container.
	var post_link = $(this).attr("data-article");
	$("#post").html('loading');
	$("#post").load(post_link);
	$('.prevFiche').hide();

	// Add Slug in location url.
	var post_Slug = $(this).attr("id");
	post_Slug = post_Slug.split("_");
	window.location.hash = post_Slug[1];
	if(post_Slug[0] == 'menu'){
	   $('.cont_projet').hide();
	   resizePage(50);
	}else{
	   resizePage(25);
	   $('.bureau').scrollTo(scroSave+'px');
	   $('.prevFiche').hide();
	}
	return false;
     });

     $.ajaxSetup({cache:false});

   }
   $('.prevFiche').hide();

     $('.fiche').mouseover(function(){
	var slugSave = $(this).attr('id');
	slugSave = slugSave.split("_");
        var slugHash = window.location.hash;
	slugHash = slugHash.substring(1);
	if(slugSave[1] != slugHash){

	   $('#post').stop().fadeOut(250);
	   $('.prevFiche').stop().fadeIn(250);
	   var prevfiche = $(this).text();
	   var prevDate = $(this).attr('data-date');
	   var prevCate = $(this).attr('data-category');
	   var colorfiche = $(this).css('background-color');
	   $('.prevFiche > h1:nth-of-type(1)').html(prevfiche);
	   $('.prevFiche > h1:nth-of-type(1)').append(prevDate);
	   // $('.prevFiche > h2').html(prevCate);
	   $('.prevFiche').css('background-color', colorfiche);
	   //Background
	   var NumbForm = $(this).parent().attr('data-numb');
	   $('.bureau').css('background-image', 'url(' + NumbForm + ')');
	}
     });

     $('.fiche').mouseleave(function(){
	$('#post').stop().fadeIn(250);
	$('.prevFiche').stop().fadeOut(250);
     });

   loadLocation();
   loadPage();

});

</script>
<script type='text/javascript' src='<?php bloginfo('url'); ?>/wp-content/plugins/ml-slider/assets/sliders/flexslider/jquery.flexslider-min.js?ver=3.3.6'></script>

  </body>
</html>
