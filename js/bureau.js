// init Isotope
var $grid = $('.bureau').isotope({
  itemSelector: '.fiche',
  layoutMode: 'fitRows',
  getSortData: {
      name: '.name',
      symbol: '.symbol',
      date: '[data-date]',
      category: '[data-category]',
    }
});

// $grid.on( 'arrangeComplete', function( event, filteredItems  ) {
//   console.log( 'arrangeComplete with ' + filteredItems.length + ' items '  );
// });
//
// bind filter button click
// $('.filter-button-group').on( 'click', 'button', function() {
//   var filterValue = $( this  ).attr('data-filter');
//   $grid.isotope({ filter: filterValue  });
// });

// bind sort button click
$('.sort-by-button-group').on( 'click', 'button', function() {
  var sortByValue = $(this).attr('data-sort-by');
  $grid.isotope({ sortBy: sortByValue  });
  if(sortByValue == 'category'){

  }
});

// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup  ) {
  var $buttonGroup = $( buttonGroup  );
  $buttonGroup.on( 'click', 'button', function() {
      $buttonGroup.find('.is-checked').removeClass('is-checked');
      $( this  ).addClass('is-checked');
  });
});
// var sortByValue = 'category';
// $grid.isotope({ sortBy: sortByValue });
